﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SnakeGame
{
    public enum Direction { Left, Right, Up, Down }

    class Settings
    {
        public static int Width { get; set; }
        public static int Height { get; set; }
        public static int Speed { get; set; }
        public static int PointsSum { get; set; }
        public static int Points { get; set; }
        public static bool GameOver { get; set; }
        public static Direction direction { get; set; }

        public Settings()
        {
            Width = 10;
            Height = 10;
            Speed = 10;
            PointsSum = 0;
            Points = 10;
            GameOver = true;
            direction = Direction.Right;

        }

    }
}
