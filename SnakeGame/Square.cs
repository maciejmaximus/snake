﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SnakeGame
{
    public class Square
    {
        public int X { get; set; }
        public int Y { get; set; }
    }
}
