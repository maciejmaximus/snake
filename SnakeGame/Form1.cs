﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SnakeGame
{
    public partial class Form1 : Form
    {
        List<Square> Snake = new List<Square>();
        Square food = new Square();

        public void StartGame()
        {
            new Settings();
            Snake.Clear();
            Settings.PointsSum = 0;
            lbEndMessage.Visible = false;

            Square head = new Square();
            head.X = 20;
            head.Y = 17;
            Square body1 = new Square();
            head.X = 19;
            head.Y = 17;
            Square body2 = new Square();
            head.X = 18;
            head.Y = 17;
            Snake.Add(head);
            Snake.Add(body1);
            Snake.Add(body2);

            GenerateFood();

            Settings.GameOver = false;
        }

        public Form1()
        {
            InitializeComponent();

            new Settings();

            timer1.Interval = 1000 / Settings.Speed;
            timer1.Tick += RefreshScreen;
            timer1.Start();
            StartGame();
        }

        void GenerateFood()
        {
            int maxXPos = pbField.Size.Width / Settings.Width;
            int maxYPos = pbField.Size.Height / Settings.Height;

            Random random = new Random();

            food.X = random.Next(0, maxXPos);
            food.Y = random.Next(0, maxYPos);
        }

        void RefreshScreen(object sender, EventArgs e)
        {
            lbPoints.Text = Settings.PointsSum.ToString();

            if (!Settings.GameOver)
            {
                if (KeyboardButtons.KeyPressed(Keys.Left) && Settings.direction != Direction.Right)
                {
                    Settings.direction = Direction.Left;
                }
                else if (KeyboardButtons.KeyPressed(Keys.Right) && Settings.direction != Direction.Left)
                {
                    Settings.direction = Direction.Right;
                }
                else if (KeyboardButtons.KeyPressed(Keys.Up) && Settings.direction != Direction.Down)
                {
                    Settings.direction = Direction.Up;
                }
                else if (KeyboardButtons.KeyPressed(Keys.Down) && Settings.direction != Direction.Up)
                {
                    Settings.direction = Direction.Down;
                }

                MoveSnake();
            }
            else
            {
                if (KeyboardButtons.KeyPressed(Keys.Enter))
                {
                    StartGame();
                }
            }

            pbField.Invalidate();
        }

        private void pbField_Paint(object sender, PaintEventArgs e)
        {
            Graphics fieldPB = e.Graphics;

            if (!Settings.GameOver)
            {
                Brush snakeColor;

                for (int i = 0; i < Snake.Count; i++)
                {
                    if (i == 0)
                    {
                        snakeColor = Brushes.Green;
                    }
                    else
                    {
                        snakeColor = Brushes.Black;
                    }

                    fieldPB.FillRectangle(snakeColor, new Rectangle(Snake[i].X * Settings.Width, Snake[i].Y * Settings.Height, Settings.Width, Settings.Height));

                    
                }
                fieldPB.FillEllipse(Brushes.Red, food.X * Settings.Width, food.Y * Settings.Height, Settings.Width, Settings.Height);
            }
            else
            {
                lbEndMessage.Text = "Koniec gry\nTwój wynik to >> " + Settings.PointsSum + " punktów\nBy spróbować ponownie wciśnij Enter";
                lbEndMessage.Visible = true;
            }
        }

        private void MoveSnake()
        {
            for (int i = Snake.Count - 1; i >= 0; i--)
            {
                if (i == 0)
                {
                    switch (Settings.direction)
                    {
                        case Direction.Left:
                            Snake[i].X--;
                            break;
                        case Direction.Right:
                            Snake[i].X++;
                            break;
                        case Direction.Up:
                            Snake[i].Y--;
                            break;
                        case Direction.Down:
                            Snake[i].Y++;
                            break;
                    }

                    int maxXPos = pbField.Size.Width / Settings.Width;
                    int maxYPos = pbField.Size.Height / Settings.Height;
                    if (Snake[0].X < 0 || Snake[0].X > maxXPos || Snake[0].Y < 0 || Snake[0].Y > maxYPos)
                    {
                        Dead();
                    }

                    for (int j = 3; j < Snake.Count; j++)
                    {
                        if (Snake[0].X == Snake[j].X && Snake[0].Y == Snake[j].Y)
                        {
                            Dead();
                        }
                    }

                    if (Snake[0].X == food.X && Snake[0].Y == food.Y)
                    {
                        Square newSnakeElement = new Square();
                        newSnakeElement.X = Snake[Snake.Count - 1].X;
                        newSnakeElement.Y = Snake[Snake.Count - 1].Y;

                        Snake.Add(newSnakeElement);

                        Settings.PointsSum += Settings.Points;

                        GenerateFood();
                    }

                }
                else
                {
                    Snake[i].X = Snake[i - 1].X;
                    Snake[i].Y = Snake[i - 1].Y;
                }
            }
        }

        private void Dead()
        {
            Settings.GameOver = true;
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            KeyboardButtons.ChangeState(e.KeyCode, true);
        }
        private void Form1_KeyUp(object sender, KeyEventArgs e)
        {
            KeyboardButtons.ChangeState(e.KeyCode, false);
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }
    }
}
