﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Windows.Forms;

namespace SnakeGame
{
    class KeyboardButtons
    {
        private static Dictionary<Keys, bool> keyTable = new Dictionary<Keys, bool>();

        public static bool KeyPressed(Keys key)
        {
            try
            {
                return (bool)keyTable[key];
            }
            catch (KeyNotFoundException)
            {
                return false;
            }
        }

        public static void ChangeState(Keys key, bool state)
        {
            keyTable[key] = state;
        }
    }
}
